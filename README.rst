IntOGen
=======

.. danger::
    The IntOGen repository on Bitbucket is currently in **read-only mode**. This means that no further **updates**, **issues**, or **pull requests** will be processed on Bitbucket.

    For the latest updates, new releases, or to contribute to the project, please refer to the official `IntOGen repository on GitHub <https://github.com/bbglab/intogen-plus>`_. All future development and maintenance will be conducted on GitHub.


.. warning::
   Please note that IntOGen requires substantial computational resources. We strongly suggest running it in a cluster environment!

Install Requirements
--------------------

1. Install `singularity <https://sylabs.io/singularity/>`_ (the pipeline has been tested with version 2.x)
2. Install `nextflow <https://www.nextflow.io/>`_ (You can use ``conda install nextflow``)
3. Clone this repository.

Download and Build Prerequisites
--------------------------------

The IntOGen pipeline requires a collection of datasets and Singularity containers in order to run.

.. warning::
   The pipeline has been tested with **hg38**, **vep92**, and **vep101**.

See the ``README.rst`` file in the ``build`` folder for further details.

You can then build all datasets from the original sources. Note that this process can take a considerable amount of time and may fail if the original sources have changed.

Run the Pipeline
----------------

The IntOGen pipeline is built on top of `Nextflow <https://www.nextflow.io/>`_. To execute the pipeline, run:

.. code-block:: bash

    nextflow run intogen.nf -resume -profile local --input test/ --output ./output

For further details, please check our documentation: http://intogen.rtfd.io/

To avoid stopping the pipeline execution due to one or a few incorrect inputs, errors in steps are ignored by default. However, we advise reviewing each error carefully to understand the underlying causes.

Licensing
---------

IntOGen uses various software tools and datasets, each with its own licensing. To accommodate all of them, the pipeline itself is released under the GNU General Public License version 3 (GPL-3.0).

If you are using IntOGen for research or academic purposes, this license should be compatible. For commercial use, please review the licenses of the individual software tools and datasets used, as some (e.g., CADD or CGC) have restrictions for commercial usage.